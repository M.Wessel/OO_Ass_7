package qtrees;

import java.io.IOException;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class GrayNode implements QTNode {

    QTNode[] children = new QTNode[4];

    public GrayNode( QTNode ... children ) {
        for (int i = 0; i < children.length && i < this.children.length; i ++ ) {
            this.children[i] = children[i];
        }
    }
    
    @Override
    public void fillBitmap( int x, int y, int width, Bitmap bitmap ) {
        int hWidth = width / 2;
        children[0].fillBitmap( x, y, hWidth, bitmap );
        children[1].fillBitmap( x + hWidth, y, hWidth, bitmap );
        children[2].fillBitmap( x + hWidth, y + hWidth, hWidth, bitmap );
        children[3].fillBitmap( x, y + hWidth, hWidth, bitmap );
    }

    @Override
    public void writeNode( Writer out ) {
        try {
            out.append( '1' );
        }
        catch( IOException ex ) {
            Logger.getLogger( GrayNode.class.getName() ).log( Level.SEVERE, null, ex );
        }
        for( QTNode child : children )
            child.writeNode( out );
    }

    public void setChild( QTNode child, int index ) {
        this.children[index] = child;
    }

    @Override
    public QTNode optimize() {
        int counter = 0;
        for( int i = 0; i < 4; i ++ ) {
            QTNode curChild = children[i].optimize();
            setChild( curChild, i );
            if( curChild instanceof WhiteLeaf )
                counter ++;
            else if( curChild instanceof BlackLeaf )
                counter --;
        }
        
        if( counter == 4 )
            return new WhiteLeaf();
        else if( counter == -4 )
            return new BlackLeaf();
        else
            return this;
    }
}
