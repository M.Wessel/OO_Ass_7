package qtrees;

import java.io.Writer;

/**
 *
 * @author Sjaak Smetsers & Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 * @version 11-03-2016
 */
public interface QTNode {

    public void fillBitmap( int x, int y, int width, Bitmap bitmap );
    public void writeNode( Writer out );
    public default QTNode optimize() {
        return this;
    }
}
