package qtrees;

import java.io.IOException;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class WhiteLeaf implements QTNode {

    @Override
    public void fillBitmap( int x, int y, int width, Bitmap bitmap ) {
        bitmap.fillArea( x, y, width, true );
    }

    @Override
    public void writeNode( Writer out ) {
        try {
            out.append( '0' );
            out.append( '1' );
        }
        catch( IOException ex ) {
            Logger.getLogger( GrayNode.class.getName() ).log( Level.SEVERE, null, ex );
        }
    }
}
