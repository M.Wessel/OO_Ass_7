package qtrees;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class QTree {

    QTNode root;

    public QTree( Reader input ) {
        try {
            root = readQTree( input );
            optimizeTree();
        }
        catch( IOException ex ) {
            Logger.getLogger( QTree.class.getName() ).log( Level.SEVERE, null, ex );
        }
    }

    public QTree( Bitmap bitmap ) {
        root = bitmap2QTree( 0, 0, bitmap.getWidth(), bitmap );
        optimizeTree();
    }

    public void fillBitmap( Bitmap bitmap ) {
        root.fillBitmap( 0, 0, bitmap.getWidth(), bitmap );
    }

    public void writeQTree( Writer sb ) {
        root.writeNode( sb );
    }

    private static QTNode readQTree( Reader input ) throws IOException {
        QTNode node = null;
        int int1;
        if( ( int1 = input.read() ) != -1 ) {
            char c = (char) int1;
            if( c == '0' ) {
                char c2 = (char) input.read();
                if( c2 == '0' )
                    node = new BlackLeaf();
                else
                    node = new WhiteLeaf();
            }
            else if( c == '1' ) {
                QTNode[] children = new QTNode[4];
//                node = new GrayNode();
//                GrayNode grayNode = (GrayNode) node;
                for( int i = 0; i < 4; i ++ )
                    children[i] = readQTree( input );
//                    grayNode.setChild( readQTree( input ), i );
                node = new GrayNode(children);
            }
        }
        return node;
    }

    public QTNode bitmap2QTree( int x, int y, int width, Bitmap bitmap ) {
        QTNode node;

        if( width > 1 ) {
            int hWidth = width / 2;
            
            node = new GrayNode( 
                    bitmap2QTree( x,          y,          hWidth, bitmap ), 
                    bitmap2QTree( x + hWidth, y,          hWidth, bitmap ),
                    bitmap2QTree( x + hWidth, y + hWidth, hWidth, bitmap ),
                    bitmap2QTree( x,          y + hWidth, hWidth, bitmap )
            );
            
//            GrayNode grayNode = (GrayNode) node;
            
//            grayNode.setChild( bitmap2QTree( x, y, hWidth, bitmap ), 0 );
//            grayNode.setChild( bitmap2QTree( x + hWidth, y, hWidth, bitmap ), 1 );
//            grayNode.setChild( bitmap2QTree( x + hWidth, y + hWidth, hWidth, bitmap ), 2 );
//            grayNode.setChild( bitmap2QTree( x, y + hWidth, hWidth, bitmap ), 3 );
        }
        else {
            if( bitmap.getBit( x, y ) )
                node = new WhiteLeaf();
            else
                node = new BlackLeaf();
        }
        return node;
    }

    private void optimizeTree() {
        if( root instanceof GrayNode )
            root = root.optimize();
    }
}
