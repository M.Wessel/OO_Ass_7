package qtrees;

import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class Qtrees {

    public static void main(String[] args) {
        //Test BitString -> QTree (+ Optimize)
        String test_tekst = "10011010001010010001010101100011000101000000";
        StringReader input = new StringReader(test_tekst);
        QTree qt = new QTree( input );

        //Test QTree -> BitString
        Writer writer = new StringWriter();
        qt.writeQTree( writer );
        System.out.println( writer );
        
        //Test QTree -> BitMap
        Bitmap bitmap = new Bitmap(8, 8);
        qt.fillBitmap( bitmap );
        System.out.println( bitmap );
        
        //Test BitMap -> QTree (+ Optimize)
        qt.bitmap2QTree( 0, 0, 8, bitmap );
        Writer writer2 = new StringWriter();
        qt.writeQTree( writer2 );
        System.out.println( writer2 );
    }
}
